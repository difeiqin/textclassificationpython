import spacy, json, os, sys
from GeoTagging.geo_tagger import *
from SectorTagging.sector_tagger import *
from CurrencyCodeTagging.currency_code_tagger import *


class MasterTagger:
    nlp = spacy.load("en_core_web_md")
    geo_tagger = GeoTagger(nlp)
    sector_tagger = SectorTagger(nlp)
    currency_tagger = CurrencyCodeTagger(nlp)

    def __init__(self):
        with open("SectorTagging/acceptance_prob.json", "r", encoding="utf-8") as f:
            acceptance_prob = json.load(f)
        self.__class__.sector_tagger.set_acceptance_prob(acceptance_prob)

    def tag(self, input_text):
        # all are sets
        result_tags = list()

        geos = self.__class__.geo_tagger.tag(input_text)
        sectors = self.__class__.sector_tagger.tag(input_text)
        currencies = self.__class__.currency_tagger.tag(input_text)
        result_tags.extend(geos)
        result_tags.extend(sectors)
        result_tags.extend(currencies)

        return result_tags

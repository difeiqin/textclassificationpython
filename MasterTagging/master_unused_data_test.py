import json, os, sys

sys.path.insert(1, "/Users/dqin/PycharmProjects/TextClassification")
from master_tagger import *
from collections import OrderedDict
from pprint import pprint
from sklearn.utils import shuffle

data_root_path = "/Users/dqin/Documents/DJ_data/organized_data"
limit = 4 * (10 ** 2)

with open(os.path.join(data_root_path, "unused_test_file_CF_lables_map.json"), "r", encoding="utf-8") as f:
    unused_CF_labels_map = json.load(f, object_pairs_hook=OrderedDict)

with open("SectorTagging/acceptance_prob.json", "r", encoding="utf-8") as f:
    acceptance_prob = json.load(f)

master_tagger = MasterTagger()
# pprint(sector_tagger.acceptance_prob)

results = OrderedDict()
for i, cur_f in enumerate(shuffle(list(unused_CF_labels_map.keys()))[:limit]):
    with open(os.path.join(data_root_path, "text", cur_f), "r", encoding="utf-8") as f:
        cur_text = f.read()
    results[cur_f] = master_tagger.tag(cur_text)
    print("Finish ", i, "-th", cur_f)

for k in results:
    print(k, results[k])

import re, yaml, os


class CurrencyCodeTagger:
    nlp = None
    single_codes = None  # dict
    code_pairs = None  # dict

    def __init__(self, nlp_reference):
        self.__class__.nlp = nlp_reference
        with open("resources/currency_codes/single_code.yml", "r", encoding="utf-8") as f:
            singles = yaml.safe_load(f)
        with open("resources/currency_codes/code_pairs.yml", "r", encoding="utf-8") as f:
            pairs = yaml.safe_load(f)

        # key: str; value: list of regex objs
        self.__class__.single_codes = {k: [re.compile(v) for v in v_list] if v_list else [] for k, v_list in
                                       singles.items()}
        self.__class__.code_pairs = {k: [re.compile(v) for v in v_list] if v_list else [] for k, v_list in
                                     pairs.items()}

    def tag(self, input_text):
        single_result = self.tag_single_code(input_text)
        pairs_result = self.tag_code_pairs(input_text)
        combined_result = single_result.copy()
        combined_result.update(pairs_result)
        return sorted([k for k in combined_result.keys() if combined_result[k]])

    def tag_single_code(self, input_text):
        exact_match = self._exact_code_match(input_text)
        description_match = self._description_match(input_text)
        return {k: exact_match[k] or description_match[k] for k in exact_match}

    def _exact_code_match(self, input_text):
        result = dict()
        for c in self.__class__.single_codes.keys():
            if len(c) != 3:  # only match the single currency code
                result[c] = False
            else:
                cur_re = re.compile("\\b" + c + "\\b")
                result[c] = True if cur_re.search(input_text) else False
        return result

    def _description_match(self, input_text):
        result = dict()
        for c, re_list in self.__class__.single_codes.items():
            result[c] = any([r.search(input_text) for r in re_list])
        return result

    def tag_code_pairs(self, input_text):
        # TODO: done by exact match; can swap position yet referring to same pair
        result = dict()
        for k, re_list in self.__class__.code_pairs.items():
            result[k] = any([r.search(input_text) for r in re_list])
        return result


if __name__ == "__main__":
    import spacy

    # nlp = spacy.load("en_core_web_md")
    currency_tagger = CurrencyCodeTagger(None)

    # Obtain all trial article ids
    article_ids = []
    pattern1 = re.compile(",")
    # with open("test_results/currency_code_tagger/sample_1.csv", "r", encoding="utf-8") as f:
    with open("test_results/currency_code_tagger/non_NA_sample_0.csv", "r", encoding="utf-8") as f:
        f.readline()  # skip 1st row
        cur_line = f.readline()
        # print(cur_line)
        while cur_line:
            article_ids.append(pattern1.split(cur_line)[1])
            cur_line = f.readline()

    print("Total number of articles:", len(article_ids))
    from collections import OrderedDict

    results = OrderedDict()
    for cur_id in article_ids:
        with open(os.path.join("/Users/dqin/Documents/DJ_data/organized_data/text", cur_id), "r") as f:
            cur_text = f.read()
            results[cur_id] = currency_tagger.tag(cur_text)

    from pprint import pprint

    for v in results.values():
        pprint(v)

    pprint(results)
# cur_text
#
# single_codes['SGD'][0]
# re1 = single_codes['SGD'][0]
# re1.findall
#
# re1.search(cur_text)
# re1 = re.compile("adsf")
# re1.findall("asfads singapore dollar alskdfjasdl f singapore dollars Singapore dollars")
# re1.search("asfads singapore dollar alskdfjasdl f singapore dollars Singapore dollars")
#
# cur_text
#
# for code in single_codes.keys():
#     print(code)
#     p2 = re.compile("\\b" + code + "\\b")
#     # p2 = re.compile(code)
#
#     print(p2.findall(cur_text))

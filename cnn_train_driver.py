from data_loader.local_loader import *
from data_loader.common_tokenizer import *
from keras_preprocessing.text import tokenizer_from_json
from cnn.embedding import *
from datetime import datetime
import re
import os.path

target_classes = ["OILG"]

if __name__ == "__main__":
    nlp = spacy.load("en_core_web_md")

    all_classes = local_loader.list_all_classes()
    data_dict = local_loader.get_classes(all_classes[:15])
    # data_dict = local_loader.get_classes(target_classes)
    df_dict = convert_to_df(data_dict)

    if os.path.exists("resources/tokenizer.json"):
        print("Trying to load pre-trained tokenizer...")
        with open("resources/tokenizer.json", "r", encoding="utf-8") as f:
            tokenizer = tokenizer_from_json(f.read())
    else:
        print("Trying to train tokenizer on training data and save...")
        tokenizer = get_common_tokenizer(df_dict)
        with open("resources/tokenizer.json", "w+", encoding="utf-8") as f:  # around 7 MB
            f.write(tokenizer.to_json())

    vocab = tokenizer.word_index
    embedding_weights = get_embedding_weights(vocab, nlp)

    EPOCHS = 10

    # for class_name in df_dict.keys():
    for class_name in target_classes:
        print("Current class: ", class_name)
        X_train, X_test, y_train, y_test = train_test_split(df_dict[class_name]['text'], df_dict[class_name]['label'],
                                                            test_size=0.1)

        PADDING_LENGTH = 500
        X_train_padded_ids = pad_sequences(tokenizer.texts_to_sequences(X_train), maxlen=PADDING_LENGTH)
        X_test_padded_ids = pad_sequences(tokenizer.texts_to_sequences(X_test), maxlen=PADDING_LENGTH)

        # -------  constructing model ------- #
        model1 = TextCNN_model_1(PADDING_LENGTH, 2, embedding_weights, max_pool_size=20)
        # model1 = TextCNN_model_original(PADDING_LENGTH, 2, embedding_weights)
        # plot_model(model1, show_shapes=True, show_layer_names=False)

        # -------  model training.... ------- #
        one_hot_labels = utils.to_categorical(y_train, 2)
        model1.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
        model1.fit(X_train_padded_ids, one_hot_labels, batch_size=100, epochs=EPOCHS)

        # ------- model prediction ------- #
        y_pred = model1.predict(X_test_padded_ids)
        pred_labels = np.argmax(y_pred, axis=1)
        time_str = re.sub('\s|:|\.',"-", str(datetime.now()))
        file_name = "_".join(["resources/model", class_name, time_str ,".h5"])
        model1.save(file_name)

        # plot_model(model1, to_file='model_001_64.png', show_shapes=True, show_layer_names=False)

        # ------- Note down the evaluation accuracy ------- #
        with open("CNN_all_classes_evaluation.txt", "a+", encoding="utf-8") as f:
            f.write(class_name + "    " + file_name)
            f.write("   Accuracy: " + str(metrics.accuracy_score(y_test, pred_labels)))
            f.write("   f1 score: " + str(metrics.f1_score(y_test, pred_labels)))
            f.write("\n")

import yaml, os

# with open("resources/regions", "r", encoding="utf-8") as f:

def get_regions_dict(all_lower_case = False):
    # Read each region's yml file, construct dict accordingly
    cf_regions = [x.rstrip(".yml") for x in os.listdir("resources/regions")]

    regions_dict = dict()
    for r in cf_regions:
        with open("resources/regions/" + r + ".yml", "r", encoding="utf-8") as f:
            regions_dict[r] = yaml.safe_load(f)

    if all_lower_case:
        lower_regions_dict = dict()
        for k, l in regions_dict.items():
            if l is None:
                lower_regions_dict[k] = []
            else:
                lower_regions_dict[k] = [v.lower() for v in l]
        return lower_regions_dict
    else:
        return regions_dict

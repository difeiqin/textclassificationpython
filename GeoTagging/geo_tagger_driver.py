import os, spacy
from collections import OrderedDict
# from geo_tagger import GeoTagger
from geo_tagger import GeoTagger

data_folder_path = "/Users/dqin/PycharmProjects/data/ready_data_http_2/ENER/test/ENER.0"
text_file_names = sorted([x for x in os.listdir(data_folder_path) if not x.startswith(".")])
nlp = spacy.load("en_core_web_md")


geo_tagger = GeoTagger(nlp)
tag_results_dict = OrderedDict()
all_geo_entities_dict = OrderedDict()

for cur_name in text_file_names:
    with open(os.path.join(data_folder_path, cur_name), "r", encoding="utf-8") as f:
        cur_text = f.read()
    tag_result = geo_tagger.tag(cur_text)
    tag_results_dict[cur_name] = tag_result
    all_geo_entities_dict[cur_name] = geo_tagger.extract_geo_entities(cur_text)
    print("Finish tagging " + cur_name)

# save tag results to local
with open("test_results/geo_tagger_debug/geo_tagging_results_3.txt", "w+", encoding="utf-8") as f:
    f.write("File name,          Tag results,          All available geo entities,\n")
    for k,v in tag_results_dict.items():
        f.write(k)
        f.write('          ')
        f.write(str(v))
        f.write('          ')
        f.write(str([GeoTagger.normalize_entity_str(str(x)) for x in all_geo_entities_dict.get(k)]))
        f.write("\n")

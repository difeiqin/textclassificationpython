from GeoTagging.load_region_yamls import *
from pprint import pprint
import re


class GeoTagger:
    nlp = None
    regions_dict = None
    re_1 = re.compile(r"^[^a-zA-Z]+")  # for starting symbols
    re_2 = re.compile(
        r"(?i)^(the|an|a)\s+")  # for starting the/an/a, has to be followed by a space, include both lower & upper case.
    re_3 = re.compile(r"([~`!@#$%^&*()\-=+,\\\s\d'\"]|'s)+$")  # ending 's, space, (expect ., which may be for abbr.)

    def __init__(self, nlp_reference):
        self.__class__.nlp = nlp_reference
        self.__class__.regions_dict = get_regions_dict(all_lower_case=True)

    def extract_geo_entities(self, input_text):
        processed_input = self.__class__.nlp(input_text)
        geo_entities = [x for x in processed_input.ents if x.label_ == "GPE"]
        return geo_entities

    def match_one_entity_to_dict(self, one_geo_entity):
        # TODO: Normalize the geo entity's text before matching?
        # e.g. South Korea's -> South Korea; The United States -> the United States
        # more such cases to be added in...
        tags = set()  # order the tags based on their appearance frequency?
        for k, v in self.regions_dict.items():  # lazy method? if the K already contained in tags, no need to match.
            if k is None or v is None:
                continue
            normalized_geo_str = self.__class__.normalize_entity_str(str(one_geo_entity.text))
            if self.match(normalized_geo_str, k, v):  # normalization + expections first handling + lowercase match
                tags.add(k)
        return tags

    def match(self, input_str, category, list_locations):
        '''
        Matching against dictionary
        :param input_str: The geo entity extracted by spaCy model
        :param category: the current category we are matching against
        :param list_locations: the list of geo locations contained under this category
        :return: boolean, as long as there is one match, return true.
        '''
        # First, take care of UK and US, (and Gaza of Israel) as they need to be capitalized
        if input_str == "UK" and category == "REGION_EQUITIES_UK":
            return True
        elif input_str == "US" and category == "REGION_EQUITIES_NA":
            return True
        elif input_str == "Gaza" and category == "REGION_EQUITIES_EM_EMA":
            return True

        if input_str.lower() in list_locations:
            return True
        return False

    @classmethod
    def normalize_entity_str(cls, entity_str):
        """
        Normalizing the input entity string, for 1> starting symbols 2> the/an/a plus at least one space 3> ending symbols
         except dot(.), which may be for abbreviation.
        :param entity_str:
        :return: the normalized result
        """
        # remove symbols(including space) * 2
        # remove the/an/a, with space * 2
        # remove ending 's, ending symbols expect (.) dot
        for i in range(2):
            result1 = cls.re_1.match(entity_str)  # use match since it is at beginning
            if result1 is not None:
                entity_str = entity_str[result1.end():]
            result2 = cls.re_2.match(entity_str)  # use match since it is at beginning
            if result2 is not None:
                entity_str = entity_str[result2.end():]
        result3 = cls.re_3.search(entity_str)  # use search here
        if result3 is not None:
            entity_str = entity_str[:result3.start()]
        return entity_str

    def match_geo_entities_to_dict(self, geo_entities):
        tags_set = set()
        # TODO: filter out the entity whose all letters are CAPITALIZED... most likely that's the correspondent's address...? OR more refined observations & rules in match function needed
        # Or filter out the geo locations that are are the very beginning or end of the text...
        for e in geo_entities:
            tags_set.update(self.match_one_entity_to_dict(e))
        return tags_set

    def tag(self, input_text):
        geo_entities = self.extract_geo_entities(input_text)
        result_tags = self.match_geo_entities_to_dict(geo_entities)
        return result_tags


if __name__ == "__main__":
    import spacy

    with open("resources/sample_news.txt", "r", encoding="utf-8") as f:
        input_text = f.read()
    nlp = spacy.load("en_core_web_md")
    geoTagger = GeoTagger(nlp)
    results = geoTagger.tag(input_text)
    pprint("The Geo tags are...")
    pprint(results)

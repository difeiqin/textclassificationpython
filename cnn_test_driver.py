from keras.models import load_model
from data_loader import local_loader
from keras_preprocessing.text import tokenizer_from_json
from keras.preprocessing.sequence import pad_sequences
import numpy as np

all_classes = local_loader.list_all_classes()
model_file_path = "resources/model_OILG_2019-09-24-17-13-19-443270_.h5"
model = load_model(model_file_path)

with open("resources/sample_news.txt", "r", encoding="utf-8") as f:
    test_text = f.read()
test_text_list = [test_text]

with open("resources/tokenizer.json", "r", encoding="utf-8") as f:
    tokenizer = tokenizer_from_json(f.read())

PADDING_LENGTH = 500
X_test_padded_ids = pad_sequences(tokenizer.texts_to_sequences(test_text_list), maxlen=PADDING_LENGTH)

y_pred = model.predict(X_test_padded_ids)
pred_labels = np.argmax(y_pred, axis=1)
print("Prediction results are ...")
print(pred_labels)
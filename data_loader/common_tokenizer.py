from keras.preprocessing.text import Tokenizer

def get_common_tokenizer(df_dict):
    tokenizer = Tokenizer()
    for k in df_dict:
        tokenizer.fit_on_texts(df_dict[k]['text'])
    return tokenizer
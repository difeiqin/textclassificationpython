import os
import pandas
import pathlib
import glob

data_path = '''/Users/dqin/PycharmProjects/data/ready_data_http_2'''
all_classes = set()


# Should have its results being cached
def list_all_classes():
    # TODO: load this variable into cache rather than global variable?
    return list(filter(lambda x: not x.startswith("."), os.listdir(data_path)))


def get_classes(class_names):
    """
    Return train&test data from local data_path, raise Error if class_names contains values that cannot be matched with local class name
    :param class_names: A string of class name or a list of class names.
    :return: A dictionary containing training & testing data for input class_names. Can be a 1-layer or 2-layer dictionary,
    depending on the number of input class names.
    """
    global all_classes  # for accessing global variable
    if len(all_classes) == 0:
        all_classes = list_all_classes()
    if isinstance(class_names, str) and class_names in all_classes:
        return __get_one_class(class_names)
    elif isinstance(class_names, list) and not list(
            filter(lambda x: x not in all_classes, class_names)):  # bool([]) == False
        result_dict = dict()
        for c in class_names:
            result_dict[c] = __get_one_class(c)
        return result_dict
    raise ValueError(
        'class_names is of incorrect type/value: ' + str(class_names) + ", While all_classes is: " + str(all_classes))


def __get_one_class(class_name):
    """
    :param class_name: one single class name
    :return: dictionary of 4 k-v pairs, keys being test_0, test_1, train_0, train_1
    """
    result_dict = dict()
    class_path = os.path.join(data_path, class_name)

    for d1 in os.listdir(os.path.join(class_path)):  # d1: train, test
        if d1.startswith("."):
            continue
        for d2 in os.listdir(os.path.join(class_path, d1)):  # d2: class.0/class.1
            if d2.startswith("."):
                continue
            cur_key = d1 + ".0" if d2.endswith("0") else d1 + ".1"
            cur_list = list()
            for doc in glob.iglob(os.path.join(class_path, d1, d2, "*")):  # look into this module
                with open(doc) as f:
                    cur_list.append(f.read())
            result_dict[cur_key] = cur_list
    return result_dict



if __name__ == "__main__":
    data = get_classes(["ENER", "AERO", "babababa"])
    print(data.keys())
    for k, v in data.items():
        print(k, len(v))

# Follow the example on DJ API page
# construct query JSON, obtain download API from DJ, when the snapshot is ready, download them.
# Decide on the format, JSON or Spark-ish file format.

import json

with open("resources/taxonoty/CF_DJ_sector_mapping_results.json", "r", encoding="utf-8") as f:
    CF_map_json = json.load(f)

CF_map_json

download_1_list = [i for k in
                   ["SECTOR_CONS_DISCR", "SECTOR_CONS_STAP", "SECTOR_ENERGY", "SECTOR_HEALTH_CARE", "SECTOR_MATERIALS",
                    "SECTOR_REAL_ESTATE", "SECTOR_TELECOM", "SECTOR_UTILITIES"] for i in CF_map_json[k]]
# Duration for list 1: 1 month, thus 20K for each category; total 150K for all these categories...

download_2_list = [i for k in ["SECTOR_FINANCIALS", "SECTOR_INDUSTRIAL", "SECTOR_IT"] for i in CF_map_json[k]]
# Duration for list 2: 2 weeks, thus 35K for each category; total 150K for three categories

total_list = [i for v in CF_map_json.values() for i in v]
# Let's download half a millon together in one call.
# Duration: one month
# OR restricted resources to "DJDN", time duration  = 9 month

with open("resources/temp_list.txt", "w+", encoding="utf-8") as f:
    json.dump(total_list, f)

# with resources taken into account, "source_code":"DJDN" returns around 60000 news per month
'''
Creating an explain: {"query": {"where": "language_code='en' AND publication_date >= '2019-01-01 00:00:00' AND publication_date <= '2019-10-01 00:00:00' AND source_code = 'DJDN'", "includes": {"industry_codes": ["i353", "i351", "iscsv", "iclt", "idurhg", "ifurn", "ihimpro", "i441", "ilgood", "iluxgds", "immari", "inondhg", "ioffeq", "i373", "i374", "i472", "i41", "ipcare", "i429", "i1", "ifinal", "i951", "iacc", "iadmin", "iadv", "i98211", "i8394", "idebtr", "i8396", "i981", "iewm", "irdops", "iinvest", "i835", "imove", "i98204", "i493", "i67", "i98205", "i8395405", "i84", "i8395414", "i98207", "i98206", "i923", "ishell", "i837", "iindstrls", "itsp", "i3dprn", "i3302022", "iadrive", "iphmetrix", "iblock", "i3302", "i3303", "impay", "ivicu", "i342", "i34531", "iint", "ibasicm", "icre", "i3441", "i7902", "iutil"]}}}
Explain Created. Job ID: 158d5e2b-97d5-49ee-9fb4-4ab6581b07c2
Explain Completed Successfully.
Number of documents returned: 549964
'''

# The necessary imports for this application
import requests
import os
from time import sleep

# The URL of the Extractions Endpoint
url = 'https://api.dowjones.com/alpha/extractions/documents'

headers = {'content-type': 'application/json', 'user-key': '0c5693277e7b2f2bcd1e0c6518e09d76'}
request_body = {
    "query": {
        "where": "language_code='en' AND publication_date >= '2019-01-01 00:00:00' AND publication_date <= '2019-10-01 00:00:00' AND source_code = 'DJDN'",
        "includes": {
            "industry_codes": [
                "i353",
                "i351",
                "iscsv",
                "iclt",
                "idurhg",
                "ifurn",
                "ihimpro",
                "i441",
                "ilgood",
                "iluxgds",
                "immari",
                "inondhg",
                "ioffeq",
                "i373",
                "i374",
                "i472",
                "i41",
                "ipcare",
                "i429",
                "i1",
                "ifinal",
                "i951",
                "iacc",
                "iadmin",
                "iadv",
                "i98211",
                "i8394",
                "idebtr",
                "i8396",
                "i981",
                "iewm",
                "irdops",
                "iinvest",
                "i835",
                "imove",
                "i98204",
                "i493",
                "i67",
                "i98205",
                "i8395405",
                "i84",
                "i8395414",
                "i98207",
                "i98206",
                "i923",
                "ishell",
                "i837",
                "iindstrls",
                "itsp",
                "i3dprn",
                "i3302022",
                "iadrive",
                "iphmetrix",
                "iblock",
                "i3302",
                "i3303",
                "impay",
                "ivicu",
                "i342",
                "i34531",
                "iint",
                "ibasicm",
                "icre",
                "i3441",
                "i7902",
                "iutil"
            ]
        }
    }
}

# Create an explain with the given query
print("Creating an explain: " + json.dumps(request_body))
response = requests.post(url + "/_explain", data=json.dumps(request_body), headers=headers)

# Check the explain to verify the query was valid and see how many docs would be returned
if response.status_code != 201:
    print("ERROR: An error occurred creating an explain: " + response.text)
else:
    explain = response.json()
    print("Explain Created. Job ID: " + explain["data"]["id"])
    state = explain["data"]["attributes"]["current_state"]

    # wait for explain job to complete
    while state != "JOB_STATE_DONE":
        self_link = explain["links"]["self"]
        response = requests.get(self_link, headers=headers)
        explain = response.json()
        state = explain["data"]["attributes"]["current_state"]

    print("Explain Completed Successfully.")
    doc_count = explain["data"]["attributes"]["counts"]
    print("Number of documents returned: " + str(doc_count))

'''
    Continue with downloading...

'''

print("Creating the snapshot: " + json.dumps(request_body))
##### REMEMBER TO SET FORMAT! CSV/JSON
request_body['query']['format'] = "csv"
response = requests.post(url, data=json.dumps(request_body), headers=headers)
print(response.text)

'''
{
    "data": {
        "attributes": {
            "current_state": "JOB_QUEUED",
            "extraction_type": "documents",
            "format": "csv"
        },
        "id": "dj-synhub-extraction-0c5693277e7b2f2bcd1e0c6518e09d76-hkf3sh4at6",
        "type": "snapshot"
    },
    "links": {
        "self": "https://api.dowjones.com/alpha/extractions/documents/dj-synhub-extraction-0c5693277e7b2f2bcd1e0c6518e09d76-hkf3sh4at6"
    }
}
'''

# Verify the response from creating an extraction is OK
if response.status_code != 201:
    print("ERROR: An error occurred creating an extraction: " + response.text)
else:
    extraction = response.json()
    print(extraction)
    print("Extraction Created. Job ID: " + extraction['data']['id'])
    self_link = extraction["links"]["self"]
    sleep(30)
    print("Checking state of the job.")

    while True:
        # We now call the second endpoint, which will tell us if the extraction is ready.
        status_response = requests.get(self_link, headers=headers)

        # Verify the response from the self_link is OK
        if status_response.status_code != 200:
            print("ERROR: an error occurred getting the details for the extraction: " + status_response.text)
        else:
            # There is an edge case where the job does not have a current_state yet. If current_state
            # does not yet exist in the response, we will sleep for 10 seconds
            status = status_response.json()

            if 'current_state' in status['data']['attributes']:
                currentState = status['data']['attributes']['current_state']
                print("Current state is: " + currentState)

                # Job is still running, Sleep for 10 seconds
                if currentState == "JOB_STATE_RUNNING":
                    print("Sleeping for 30 seconds... Job state running")
                    sleep(30)

                elif currentState == "JOB_VALIDATING":
                    print("Sleeping for 30 seconds... Job validating")
                    sleep(30)

                elif currentState == "JOB_QUEUED":
                    print("Sleeping for 30 seconds... Job queued")
                    sleep(30)

                elif currentState == "JOB_CREATED":
                    print("Sleeping for 30 seconds... Job created")
                    sleep(30)

                else:
                    # If currentState is JOB_STATE_DONE then everything completed successfully
                    break



with open("resources/DJ_data/download_links.json", "r", encoding="utf-8") as f:
    status = json.load(f)

if status['data']['attributes']['current_state'] == "JOB_STATE_DONE":
    print("Job completed successfully")
    print("Downloading snapshot files to current directory")
    for file in status['data']['attributes']['files']:
        filepath = file['uri']
        parts = filepath.split('/')
        filename = parts[len(parts) - 1]
        r = requests.get(file['uri'], stream=True, headers=headers)
        dir_path = "resources/DJ_data"
        filename = os.path.join(dir_path, filename)
        with open(filename, 'wb') as fd:
            for chunk in r.iter_content(chunk_size=128):
                fd.write(chunk)
                # job has another state that means it was not successful.
        print("End of file", filename)

import json, os, copy
import pandas as pd
from sklearn.utils import shuffle

data_root_path = "/Users/dqin/Documents/DJ_data/organized_data"
N = 10 ** 4
seed = 0

with open("resources/taxonoty/CF_DJ_sector_mapping_results.json", "r", encoding="utf-8") as f:
    CF_map_json = json.load(f)
with open(os.path.join(data_root_path, "matched_file_names.json"), "r", encoding='utf-8') as f:
    matched_CF = json.load(f)
with open(os.path.join(data_root_path, "mismatched_file_names.json"), "r", encoding='utf-8') as f:
    mismatched_CF = json.load(f)

unused_files = dict()

for k in matched_CF:
    cur_set = set()
    cur_set.update(shuffle(matched_CF[k], random_state=seed)[N:])
    cur_set.update(shuffle(mismatched_CF[k], random_state=seed)[N:])
    unused_files[k] = copy.deepcopy(cur_set)

for k in unused_files:
    print(len(unused_files[k]))  # should be of same number

# the common set of unused artiles
common_unused = unused_files[list(unused_files.keys())[0]]
# len(common_unused.intersection(common_unused))

for k in unused_files:
    common_unused = unused_files[k].intersection(common_unused)

print(len(common_unused))  # the set of data never used in any training at all


# find the labels for each of the unused files, by going back to matched_CF & mismatched_CF

def find_keys(news_id, ref_dict):
    result = []
    for k in ref_dict.keys():
        if news_id in ref_dict[k]:
            result.append(k)
    return result


matched_CF_sets = dict()
for k in matched_CF:
    matched_CF_sets[k] = set(matched_CF[k])

unused_CF_labels_map = dict()
for id in common_unused:
    unused_CF_labels_map[id] = find_keys(id, matched_CF_sets)

print(unused_CF_labels_map)  # inspection

with open(os.path.join(data_root_path, "unused_test_file_CF_lables_map.json"), "w", encoding="utf-8") as f:
    json.dump(unused_CF_labels_map, f)

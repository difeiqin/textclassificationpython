# read in downloaded file, if it contains the tag(+ve), put them in one folder; Otherwise it is -ve, note down the names
# And sample the same amount as +ve has, put in another folder
# save the file in such format: each piece of text is in a separate file, and (somehow) we need to note down its labels
# use the on_epoch_end() to specify the shuffling behavior.
# So, a structure of the data folder should be:
# |data_root
# |- class_1
#   |- train
#       |- positive_data_folder
#           |- news_text_1
#           |- news_text_2
#       |- negative_data_folder
#           |- news_text_3
#           |- news_text_4
#   |- test
# ... similar structure
# |- class_2
# ... same as above...
# |- class_3

# To train on training_generator, x_i and y_i shall be bundled together and returned by the training_generator.

# remember to do the train-test split


# Let do an experiment first with the temp_executive csv file
# using training_generator

import csv
from os import path

import os, json
import pandas as pd

target_folder_path = "/Users/dqin/Documents/DJ_data/organized_data"
csv_path = "/Users/dqin/Documents/DJ_data/csv"
file_names = sorted([fn for fn in filter(lambda x: not x.startswith("."), os.listdir(csv_path))])
chunk_size = 10 ** 3

'''
Index(['copyright', 'subject_codes', 'art', 'modification_datetime', 'body',
       'company_codes_occur', 'company_codes_about', 'company_codes_lineage',
       'snippet', 'publication_date', 'market_index_codes', 'credit',
       'section', 'currency_codes', 'region_of_origin', 'ingestion_datetime',
       'modification_date', 'source_name', 'language_code', 'region_codes',
       'company_codes_association', 'person_codes', 'byline', 'dateline',
       'company_codes_relevance', 'source_code', 'an', 'word_count',
       'company_codes', 'industry_codes', 'title', 'publication_datetime',
       'publisher_name', 'action', 'document_type'],
      dtype='object')
'''


# Make use of 'industry_codes' column
def filter_logic(input_row):
    '''

    :param input_row: a row from the news dataframe
    :return: bool (true/false) whether the row should be kept. Kept if true; drop if false
    '''
    if input_row['word_count'] < 200 or len(input_row['industry_codes']) < 2:
        return False
    return True


def map_and_save(df, mapping, save_folder_path, matched_result, mismatched_result):
    '''

    :param df: the current processing dataframe
    :param mapping: a dict of mapping b/t CF and DJ industry codes; k:string, v:set()
    :param save_folder_path: folder path for saving text
    :param matched_result: a dictionary with keys:CF_categories; values: list of mapped/matched text file names
    :param mismatched_result: a dictionary with keys:CF_categories; values: list of mismatched text file names
    :return: Null
    '''
    # change mapping to k -> set()
    for i, r in df.iterrows():
        industry_str = r['industry_codes']
        file_name = str(r['an'])

        # save body as a separate text file
        with open(os.path.join(save_folder_path, "text", file_name), "w", encoding="utf-8") as f:
            f.write(r['title'] if isinstance(r['title'], str) else str(r['title']))
            f.write('\n')
            f.write(r['body'] if isinstance(r['body'], str) else str(r['body']))

        if not filter_logic(r):
            continue
        # do label matching
        industry_codes = set(x for x in filter(lambda x: bool(x), industry_str.split(",")))  # filter out the empty ones
        matched_CF_codes = [k for k, v in mapping.items() if bool(industry_codes.intersection(v))]
        mismatched_CF_codes = [x for x in set(mapping.keys()).difference(set(matched_CF_codes))]

        for k in matched_CF_codes:
            matched_result[k].append(file_name)
        for k in mismatched_CF_codes:
            mismatched_result[k].append(file_name)
    print("End of processing current dataframe of", chunk_size, "rows.")


# play with df
def convert_dict_list_to_set(input_dict):
    new_dict = dict()
    for k, v in input_dict.items():
        if not isinstance(v, list):
            raise ValueError("ERROR! values in the input_dict shall be of type list, yet it is", type(v))
        new_dict[k] = set(v)
    return new_dict


def save_intermediate_result(filepath, matched_result, mismatched_result):
    with open(os.path.join(filepath, "temp", "matched_file_names.json"), "w", encoding="utf-8") as f:
        json.dump(matched_result, f)
    with open(os.path.join(filepath, "temp", "mismatched_file_names.json"), "w", encoding="utf-8") as f:
        json.dump(mismatched_result, f)


# load the CF mapping here
if __name__ == "__main__":
    with open("resources/taxonoty/CF_DJ_sector_mapping_results.json", "r", encoding="utf-8") as f:
        CF_mapping = json.load(f)
    # create_mapped_folder(CF_mapping, data_root_path)
    CF_mapping = convert_dict_list_to_set(CF_mapping)
    matched_result = {k: [] for k in CF_mapping.keys()}
    mismatched_result = {k: [] for k in CF_mapping.keys()}

    for cur_f in file_names:
        for df in pd.read_csv(os.path.join(csv_path, cur_f), chunksize=chunk_size, header=0):
            map_and_save(df, CF_mapping, target_folder_path, matched_result, mismatched_result)
            save_intermediate_result(target_folder_path, matched_result, mismatched_result)
        print("End of mapping  file", cur_f)

    # save the matched and mismatched result to local
    with open(os.path.join(target_folder_path, "matched_file_names.json"), "w+", encoding="utf-8") as f:
        json.dump(matched_result, f)
    with open(os.path.join(target_folder_path, "mismatched_file_names.json"), "w+", encoding="utf-8") as f:
        json.dump(mismatched_result, f)

# DOES not here as each entry of CSV actually occupies more than one row...
# Thus you have to use pandas dataframe for handling such wiredo csv
# def executive_generator(filepath):
#     file_name = path.basename(filepath)
#     with open(filepath, "r") as csvf:
#         datareader = csv.reader(csvf)  # different from pandas.read_csv, which loads everything into memory; here it returns an iterator for each row
#         yield next(datareader)  # yield the headers first. You may print it for your reference
# count = 0
# for i, row in enumerate(datareader):
#     if "Tim Cook".lower() in  row[2].lower():
#         yield (i,row)
#         count += 1
# return

# news_generator = executive_generator(filepaths[0])
# for i in news_generator:
#     print(i)
#     news_generator.__next__()

# for i in executive_generator(filepaths[0]):
#     print(i)


# import pandas as pd
#
# chunksize = 10 ** 3
# for chunk in pd.read_csv(filepaths[0], chunksize=chunksize):
#     print(chunk.shape)
#
# a = pd.read_csv(filepaths[0], chunksize=chunksize)
# type(a)
#
# chunk.iloc[:3, ].columns

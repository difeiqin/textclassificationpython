import pandas as pd
from collections import OrderedDict
import json
from pprint import pprint

df = pd.read_csv("resources/taxonoty/CF_DJ_sector_mapping.csv", header=0)

# target column: Mapped Categories
kept_df = df.dropna(subset = ['Mapped Categories'])[["fCode (IN=Code)", "Descriptor", "Description", "Mapped Categories"]]
kept_df.shape  # (66, 4)

all_sectors = set(kept_df["Mapped Categories"].values)
# Construct map based on mapping

sector_mapping = OrderedDict()
for k in sorted(all_sectors):
    sector_mapping[k] = list(kept_df[kept_df["Mapped Categories"] == k]["fCode (IN=Code)"].values)

with open("resources/taxonoty/CF_DJ_sector_mapping_results.json", "w+", encoding="utf-8") as f:
    json.dump(sector_mapping, f)
import pandas as pd
import sys, os
import numpy as np

CHUNK_SIZE = 10 ** 4
data_root_path = "/Users/dqin/Documents/DJ_data/csv"
valid_fns = list(filter(lambda x: x.endswith(".csv"), os.listdir(data_root_path)))

CC_counts = 0  # currency code counts
CC_df = None

for fn in valid_fns:
    for cur_df in pd.read_csv(os.path.join(data_root_path, fn), chunksize=CHUNK_SIZE, header=0):
        if CC_df is None:
            CC_df = pd.DataFrame(columns=cur_df.columns)
        print("Current shape of remaining cur_df", cur_df[cur_df['currency_codes'].notna()].shape)
        # CC_df = pd.concat([CC_df, cur_df[cur_df['currency_codes'].notna()]])
        CC_df = pd.concat([CC_df, cur_df[cur_df['currency_codes'].isna()]])
    break
CC_df.reset_index(inplace=True)
CC_df.drop(['index'], axis=1, inplace=True)
print("Final shape of CC_df", CC_df.shape)  # (9839, 35)

# Let's go through them and analyze a little bit... take a sample of 30
SAMPLE_SIZE = 100
# SAMPLE_SIZE = 30
sample_1 = CC_df.sample(SAMPLE_SIZE, random_state=1)[["an", "currency_codes"]]  # see sample_names.txt

# save the sample_1 and codes into local csv
sample_1.to_csv("test_results/currency_code_tagger/non_NA_sample_0.csv")

# Gather all the currency code
all_codes = sorted(set(x for v in CC_df["currency_codes"].values for x in filter(lambda x: len(x) >= 2, v.split(","))))
all_codes.__len__()  # 114
print(all_codes)  # what's the length of currency code?
codes_len = dict()
for c in all_codes:
    codes_len.setdefault(len(c), []).append(c)

print(codes_len.keys())  # i.e. each currency's code has to be length 3; or currency pair, length 6
codes_len[3].__len__()  # 91 currency code
codes_len[6].__len__()  # 23 currency pairs

# gather all the article ids from sample_names.txt

article_ids = []
import re

pattern1 = re.compile("\s")
with open("test_results/currency_code_tagger/sample_names.txt", "r", encoding="utf-8") as f:
    f.readline()  # skip 1st row
    cur_line = f.readline()
    while cur_line:
        article_ids.append(pattern1.split(cur_line)[1])
        # print(pattern1.split(cur_line))
        cur_line = f.readline()

print(article_ids)

for text_id in article_ids:
    with open(os.path.join("/Users/dqin/Documents/DJ_data/organized_data/text", text_id), "r", encoding="utf-8") as f:
        full_text = f.read()
        print(full_text)
        break


import requests

link = "https://api.dowjones.com/taxonomies/executives/csv"
headers = {'content-type': 'application/json', 'user-key': '0c5693277e7b2f2bcd1e0c6518e09d76'}

executives_result = requests.get(link, headers = headers)
executives_result.status_code

result_json = executives_result.content

print("end")

with open("resources/temp_executives.csv", "wb") as f:
    f.write(executives_result.content)
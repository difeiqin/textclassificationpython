import json
import pprint

category_file_path = "/Users/dqin/Downloads/Commonfinance Categories.json"

with open(category_file_path, "r", encoding="utf-8") as f:
    raw_cat = json.load(f)  # 317

comm_cat = list(filter(lambda x: "family" in x.keys() and not x['family'].lower().startswith("main"),
                       raw_cat))  # len(comm_cat) = 199
families = set([x['family'] for x in comm_cat])

cf_categories_dict = dict()
for cur_item in comm_cat:
    cur_cats = cf_categories_dict.setdefault(cur_item['family'], [])
    cur_cats.append(cur_item['key'])

# print to check...
for k in cf_categories_dict.keys():
    print("\n")
    print(k)
    pprint.pprint(cf_categories_dict[k])

# Two things I can work on for now, one is SECTOR, the other REGION.
'''
    SECTOR: mapping training data from Refinitiv.
    REGION: 11 regions. Dictionary method, using the dow jones file.
'''

## Details of each category

# REGION
# INSURANCE_TYPE
# PRODUCT_TYPE
# AREA
# ASSET_CLASS
# RISK_CURRENCY_GROUP
# SECTOR
# RISK_COUNTRY
# OBJECTIVES

all_categories = ["REGION", "INSURANCE_TYPE", "PRODUCT_TYPE", "AREA", "ASSET_CLASS", "RISK_CURRENCY_GROUP", "SECTOR", "RISK_COUNTRY", "OBJECTIVES"]

for cat in all_categories:
    details = list(filter(lambda x: x['family'].upper() == cat, comm_cat))
    print((" " + cat + " ").center(40, "="))
    for r in details:
        pprint.pprint(r['key'] + "   " + r['name'])
    print("\n\n")


import json
import pandas as pd
from pprint import pprint
import numpy as np

# read files from local
# with open("resources/taxonoty/mapping_DJ_to_CF.csv", "r", encoding="utf-8") as f:

df = pd.read_csv("resources/taxonoty/CF_DJ_product_asset_mapping_updated.csv", header=0)

df.shape
df.columns

l1_product = set(df['L1 Product Type  Mapping'].dropna().values)
l2_product = set(df['L2 Product Type  Mapping'].dropna().values)
l1_asset = set(df['L1 Asset Class Mapping'].dropna().values)
l2_asset = set(df['L2 Asset Class Mapping'].dropna().values)

map_list = [{}, {}, {}, {}]
for i, cur_f in enumerate([l1_product, l2_product, l1_asset, l2_asset]):
    for cur_cat in cur_f:  # construct mapping for each cat
        # print(cur_cat)
        # print(df[df.iloc[:, 5 + i] == cur_cat].iloc[:, 0])
        map_list[i][cur_cat] = list(df[df.iloc[:, 5 + i] == cur_cat].iloc[:, 0].values)

# in total, how many labels are mapped?
sum([len(x) for x in map_list])  # 29. i.e. all together 29 categories got mapped successfully.

with open("resources/taxonoty/CF_DJ_product_asset_mapping_results.json", "w+", encoding="utf-8") as f:
    json.dump(map_list, f)
    # for m in map_list:
    #     pprint(m, f)


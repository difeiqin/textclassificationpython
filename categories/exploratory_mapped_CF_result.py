import json, os

target_path = "/Users/dqin/Documents/DJ_data/organized_data"

with open(os.path.join(target_path, "matched_file_names.json"), "r", encoding="utf-8") as f:
    matched_CF = json.load(f)

with open(os.path.join(target_path, "mismatched_file_names.json"), "r", encoding="utf-8") as f:
    mismatched_CF = json.load(f)

for k in matched_CF.keys():
    print(k, "total text under this matched is...", len(matched_CF[k]))

for k in mismatched_CF.keys():
    print(k, "total text under this matched is...", len(mismatched_CF[k]))

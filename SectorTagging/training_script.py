import spacy, sys
from keras.models import load_model
sys.path.insert(1, "/Users/dqin/PycharmProjects/TextClassification")

# Either change the folder structure s.t. everything is under one project; or move to jungle and use its framework
# import cnn
import datetime
from sector_data_generator import *
from keras_preprocessing.text import tokenizer_from_json
from cnn.embedding import *

if __name__ == "__main__":
    nlp = spacy.load("en_core_web_md")
    with open("SectorTagging/shared_tokenizer.json", "r", encoding='utf-8') as f:
        tokenizer = tokenizer_from_json(f.read())

    training_generator = SectorDataGenerator(128, tokenizer)
    validation_generator = SectorDataGenerator(128, tokenizer, N=500, for_evalutation=True)

    with open("SectorTagging/shared_tokenizer.json", "r", encoding='utf-8') as f:
        tokenizer = tokenizer_from_json(f.read())
    vocab = tokenizer.word_index
    PADDING_LENGTH = 500  # thus input size

    # For now, train on the current tag only.
    embedding_weights = get_embedding_weights(vocab, nlp)

    EPOCHS = 5

    '''
        Load to continue training or start new training ???
    '''
    model1 = TextCNN_model_1(PADDING_LENGTH, 2, embedding_weights, max_pool_size=20)
    model1.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
    training_generator.current_class

    # model1 = load_model("resources/tagging_models/model_2019-10-18_22_41_19.h5")
    model1.fit_generator(generator=training_generator, validation_data=validation_generator,
                         epochs=EPOCHS, use_multiprocessing=True)

    # with open("tagging_models/first_model.h5", "w") as f:

    cur_time_str = str(datetime.datetime.now()).split(".")[0].replace(":", "_").replace(" ", "_")
    model1.save("resources/tagging_models/" + str(training_generator.current_class) + "_model_" + cur_time_str + ".h5")
    # Finish the test data training_generator;
    # Finish the saving part
    # change the evaluation script

    # model1.fit(X_train_padded_ids, one_hot_labels, batch_size=100, epochs=EPOCHS)

    # for class_name in target_classes:
    #     print("Current class: ", class_name)
    #     X_train, X_test, y_train, y_test = train_test_split(df_dict[class_name]['text'], df_dict[class_name]['label'],
    #                                                         test_size=0.1)
    #
    #     # -------  constructing model ------- #
    #     # model1 = TextCNN_model_original(PADDING_LENGTH, 2, embedding_weights)
    #     # plot_model(model1, show_shapes=True, show_layer_names=False)
    #
    #     # -------  model training.... ------- #
    #     one_hot_labels = utils.to_categorical(y_train, 2)
    #     model1.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
    #     model1.fit(X_train_padded_ids, one_hot_labels, batch_size=100, epochs=EPOCHS)
    #
    #     # ------- model prediction ------- #
    #     y_pred = model1.predict(X_test_padded_ids)
    #     pred_labels = np.argmax(y_pred, axis=1)
    #     time_str = re.sub('\s|:|\.',"-", str(datetime.now()))
    #     file_name = "_".join(["resources/model", class_name, time_str ,".h5"])
    #     model1.save(file_name)
    #
    #     # plot_model(model1, to_file='model_001_64.png', show_shapes=True, show_layer_names=False)
    #
    #     # ------- Note down the evaluation accuracy ------- #
    #     with open("CNN_all_classes_evaluation.txt", "a+", encoding="utf-8") as f:
    #         f.write(class_name + "    " + file_name)
    #         f.write("   Accuracy: " + str(metrics.accuracy_score(y_test, pred_labels)))
    #         f.write("   f1 score: " + str(metrics.f1_score(y_test, pred_labels)))
    #         f.write("\n")

# Build tokenizer for each of the classed
# And save to local, so they can be re-used later.
# For now, just use the keras tokenizer.
# If you have time, use spacy & stop word removal
import os
import pandas as pd
import numpy as np
from keras.preprocessing.text import Tokenizer
import keras.preprocessing.text as text

data_root_path = "/Users/dqin/Documents/DJ_data/organized_data"
csv_path = "/Users/dqin/Documents/DJ_data/csv"

file_names = sorted([fn for fn in filter(lambda x: not x.startswith("."), os.listdir(csv_path))])
chunk_size = 10 ** 3

# You can directly iterate over the dataframe(csv) rather than each individual text file
tokenizer = Tokenizer()

for fn in file_names:
    cur_chunk_index = 0
    with open(os.path.join(csv_path, fn), "r", encoding="utf-8") as f:
        for df in pd.read_csv(f, header=0, chunksize=chunk_size):
            cur_chunk_index += 1
            index = np.logical_and(df['word_count'] > 200, df['body'] is not np.nan)
            tokenizer.fit_on_texts(df[index]['body'].values)
            print("finish chunk:", cur_chunk_index, "-th")
            del (df)
    print("... Finish one file... ")

with open(os.path.join("SectorTagging", "shared_tokenizer.json"), "w") as f:
    f.write(tokenizer.to_json())

len(tokenizer.word_index)

# def get_common_tokenizer(df_dict):
#     tokenizer = Tokenizer()
#     for k in df_dict:
#         tokenizer.fit_on_texts(df_dict[k]['text'])
#     return tokenizer
#
#
# def get_common_tokenizer(df_dict):
#     for k in df_dict:
#         tokenizer.fit_on_texts(df_dict[k]['text'])
#     return tokenizer
#
#
# with open(os.path.join())
#     test = "--Dawn Lim(END) Dow Jones Newswires January 01, 2019 10:00 ET (15:00 GMT)"
#
#     test = '''The Money Managers to Watch in 2019
# On the passive-investment side, a price war is afoot. Index-fund managers are all nearing zero in fees, with questions growing about what these firms can do next to deliver inflows. There is also a question of what they'll do if the weakness in stocks that took hold at the end of last year continues into 2019.
# And then there's BlackRock Inc., the world's largest asset manager. Whatever BlackRock does will make waves for everyone else in the business.
# As 2019 gets going, here are the money managers and executives to watch.
# David Einhorn
# David Einhorn's slide continued in 2018. Coming off six straight years in which his Greenlight Capital Inc. lagged behind the total-return of the S&P 500, Mr. Einhorn needed a win last year.
# Instead, Greenlight lost almost 28% for the year through November, compared with a 5.1% total-return by the S&P 500. Stock hedge funds were down an average 3.3% for the period, according to research firm HFR.
# "The market appears to be rejecting our entire strategy of value investing," Greenlight said in its third-quarter letter to clients. "Value investing" refers to a preference for stocks considered inexpensive relative to earnings and other metrics.
# The value of an investment in Greenlight from 2009 through the end of November 2018 rose about 48%, an analysis by The Wall Street Journal found. The S&P 500 rose roughly 277% total over the same period.
# Even shareholders of Greenlight Capital Re, the reinsurance vehicle Greenlight controls, have suffered. Its stock price closed at $8.62 on Monday, down from $20.10 at the end of 2017.
# As Mr. Einhorn's struggles continue, clients have become restive. At least one has publicly cited Mr. Einhorn's divorce process as a factor in the firm's weak performance. Mr. Einhorn dismissed that claim in a Journal article in July.
# Defections have helped to shrink Greenlight to less than $5.5 billion in assets under management, from $12 billion in 2014.
# Yet Mr. Einhorn has no plans to close and return investors' money, said a person familiar with the firm. Investors and others in the industry are once again watching to see if his luck turns in 2019.
# Mr. Einhorn shot to fame during the financial crisis with a prescient 2008 call against Lehman Brothers Holdings Inc. His clout was such that he was able to move a company's stock price with a few probing questions on an investor call or with a fast-moving presentation at a hedge-fund conference.
# Heading into the new year, Mr. Einhorn retains some advantages not enjoyed by other hedge-fund managers that closed shop in 2018. Those include money from Greenlight's reinsurance vehicle and more than $1 billion of his own cash in the fund, according to some clients.
# --Juliet Chung
# Daniel Sundheim
# Daniel Sundheim stood out at a time when many hedge-fund managers struggle to raise money. His New York-based D1 Capital Partners amassed about $5 billion last year.
# The firm gained star power, too. Jeremy Katz, who was deputy director of the Trump White House's National Economic Council and previously worked in the George W. Bush administration, was named D1's chief operating officer. Mr. Sundheim himself was previously the chief investment officer at Viking Global Investors, a prominent hedge fund.
# Still, Mr. Sundheim's successful fundraising doesn't change the simple reality that the hedge-fund model is under pressure.
# Hedge funds typically charge higher fees than other money managers and tout their ability to do better when markets turn volatile. Yet over the last decade they have trailed low-cost, passive investment products that track indexes like the S&P 500.
# The average hedge fund, across strategies, lost 2% through November, according to research firm HFR. Many hedge funds have struggled to justify their fees, with some deciding to return money to clients and close.
# So far, Mr. Sundheim's performance has been uneven. The fund lost as much as 8.4% in October alone for some clients. In November, D1 made back much of its losses, finishing up about flat as of the end of November.
# D1 primarily makes bets for and against stocks and has a large chunk of its portfolio in private companies. Private investments include Instacart and Juul, the e-cigarette maker.
# People who have invested in D1 say they are making a long-term, multiyear bet on Mr. Sundheim. As the calendar turns to 2019, investors will be watching closely to see if he can make good on that bet.
# --Rachael Levy
# Kathleen Murphy
# Kathleen Murphy will celebrate her 10-year anniversary at Fidelity Investments on Wednesday, marking a decade that saw the firm complete its transformation from a savvy stock picker to an investing behemoth that helps people more broadly manage their investing dollars.
# Underpinning that change has been Fidelity's embrace of low-cost index funds, which the money manager's former chairman, Ned Johnson, once derided as offering investors only average returns.
# Ms. Murphy, president of Fidelity's personal-investing division, has played a big role in the shift. The 55-year-old executive's remit spans everything from brokerage and individual-retirement accounts to the team that devises new investment products. In all, her business oversees more than $2 trillion and some 15,000 employees.
# Ms. Murphy got her start as a lawyer at Aetna Inc., where she rose to become general counsel and chief administrative officer of a financial-services arm the insurer sold to ING Groep NV in 2000.
# "I've been fatalistic about my career from the beginning," Ms. Murphy said, noting she had never sought a position outside the legal department. She ultimately made the switch to a revenue-producing role after former ING Americas chief Tom McInerney appointed her head of the financial firm's U.S. institutional business.
# Fidelity hired her in January 2009.
# Thanks in part to strong demand from Ms. Murphy's clients, assets at Fidelity's index-investing business have surged from $60.4 billion in 2008 to $424.4 billion at the end of November. The growth has helped offset client losses from the more-expensive stock and bond funds.
# Those assets figure to rise even higher thanks to a series of announcements Ms. Murphy made on Aug. 1: Fidelity eliminated minimum account balances, slashed fees on its lineup of index funds and began offering funds with no fees to brokerage clients.
# While the firm's "Zero" funds launch drew the most attention, Ms. Murphy said the fee cuts on 21 existing index funds made the bigger impact. Indeed, Fidelity's picked up 68% of the new money that flowed into all index mutual funds in November, she said, citing data from SimFund.
# The decision to undercut index pioneer Vanguard Group on cost may have once seemed unfathomable inside Fidelity's Boston headquarters. But by this summer, it took Ms. Murphy and her team a little more than hour to make their case to current Fidelity chairman Abigail Johnson.
# "She approved it that day," Ms. Murphy said.
# --Justin Baer
# Mark Wiseman
# Mark Wiseman is the face of BlackRock's bid to join the big leagues of private-equity deal makers. The senior managing director is overseeing efforts to raise $12 billion to take direct stakes in private companies, said people familiar with the matter.
# The initial task is to convince financial institutions to entrust private-equity investing to BlackRock, a firm that became the world's biggest asset manager because of exchange-traded funds and index funds that mimic markets. BlackRock currently has only a small piece of the burgeoning private markets.
# The push in private equity is just one part of BlackRock's roughly $100 billion alternatives investments business, of which Mr. Wiseman is chairman. It reflects top management's ambitions to make private investments -- such as infrastructure, private equity and credit -- a bigger chunk of BlackRock's future profits. Chief Executive Laurence Fink, in a recent call with analysts, expressed hopes that these types of illiquid investments would be "an ever-growing contributor to our financial results over time." These strategies lock in investor capital for the long haul, command higher fees than basic stock-and-bond portfolios -- and are another way BlackRock can influence how companies are run.
# The move also gives Mr. Wiseman, one of Mr. Fink's potential successors, a new way to leave his mark at BlackRock. Mr. Wiseman has been at the company since 2016, when he was hired from the Canada Pension Plan Investment Board. In his first big move as BlackRock's active equities head, Mr. Wiseman overhauled the unit that runs active stock-investing strategies. He has since led a restructuring of the roughly $300 billion division, which involved layoffs, pricing changes and a greater reliance on computer models to drive investments.
# Today, the former pension chief is often part of the firm's interactions with major sovereign-wealth funds and pensions, institutions vital to BlackRock's push into direct investing.
# With net inflows slowing, price wars raging and many of their stocks lagging, asset managers including BlackRock need to find new ways to expand. This means Mr. Wiseman's task of helping shape the firm's higher-margin businesses is now increasingly important to BlackRock's future.
# --Dawn Lim
# (END) Dow Jones Newswires
# January 01, 2019 10:00 ET (15:00 GMT)'''
#
# # text.text_to_word_sequence(test, lower=False)
# #
# # text.text_to_word_sequence(test)
# #
# # result = nlp(test)
# # for x in result:
# #     if str(x) == "U.S.":
# #         print(x)

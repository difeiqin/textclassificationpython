import spacy, sys, datetime
from keras.models import load_model

sys.path.insert(1, "/Users/dqin/PycharmProjects/TextClassification")

# Either change the folder structure s.t. everything is under one project; or move to jungle and use its framework
# import cnn
from sector_data_generator import *
from keras_preprocessing.text import tokenizer_from_json
from cnn.text_cnn_small import *

if __name__ == "__main__":
    nlp = spacy.load("en_core_web_md")
    with open("SectorTagging/shared_tokenizer.json", "r", encoding='utf-8') as f:
        tokenizer = tokenizer_from_json(f.read())

    EPOCHS = 15
    PADDING_LENGTH = 500  # thus input size
    EMBEDDING_DIM = 300

    training_generator = SectorDataGenerator(128, PADDING_LENGTH, tokenizer, nlp)
    validation_generator = SectorDataGenerator(128, PADDING_LENGTH, tokenizer, nlp, N=2000, for_evalutation=True)

    # Load to continue training or start new training ???
    for cur_class in training_generator.all_classes:
        # if cur_class in ["SECTOR_CONS_DISCR", "SECTOR_CONS_STAP", "SECTOR_ENERGY", "SECTOR_FINANCIALS",
        #                  "SECTOR_HEALTH_CARE", "SECTOR_INDUSTRIAL", "SECTOR_IT",
        #                  "SECTOR_MATERIALS"]:  # already trained.
        #     continue

        training_generator.set_current_tag(cur_class)
        validation_generator.set_current_tag(cur_class)
        model1 = TextCNN_model_small(PADDING_LENGTH, 2, EMBEDDING_DIM, max_pool_size=20)
        model1.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])

        # model1 = load_model("resources/tagging_models_small/SECTOR_CONS_DISCR.2019-10-22_13_51_12.h5")
        print("-" * 100)
        print("Current training class:", training_generator.current_class)
        model1.fit_generator(generator=training_generator, validation_data=validation_generator,
                             epochs=EPOCHS, use_multiprocessing=True)

        # with open("tagging_models/first_model.h5", "w") as f:

        cur_time_str = str(datetime.datetime.now()).split(".")[0].replace(":", "_").replace(" ", "_")
        print("-" * 100)
        model1.save(
            "resources/tagging_models_small/" + str(training_generator.current_class) + "." + cur_time_str + ".h5")

from keras.utils import Sequence, to_categorical
from word_embedder import *
from sklearn import utils
from keras_preprocessing.sequence import pad_sequences
import numpy as np
import os, json, datetime


class SectorDataGenerator(Sequence):
    def __init__(self, batch_size, padding_length, tokenizer, nlp_reference, N=10 ** 4, shuffle=True, seed=0, for_evalutation=False):
        self.data_root_path = "/Users/dqin/Documents/DJ_data/organized_data"
        self.tokinizer = tokenizer
        self.N = N  # no. of training data for each class
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.PADDING_LENGTH = padding_length
        self.embedder = WordEmbedder(nlp_reference, self.tokinizer)

        with open(os.path.join(self.data_root_path, "matched_file_names.json"), "r", encoding='utf-8') as f:
            matched_CF = json.load(f)
        with open(os.path.join(self.data_root_path, "mismatched_file_names.json"), "r", encoding='utf-8') as f:
            mismatched_CF = json.load(f)

        self.all_classes = list(matched_CF.keys())
        if for_evalutation is False:  # For training data
            self.data = {k: np.array(utils.shuffle(matched_CF[k], random_state=seed)
                                     [:self.N] + utils.shuffle(mismatched_CF[k], random_state=seed)[:self.N])
                         for k in self.all_classes}
        else:  # for evalutation data, extract from bottom
            self.data = {k: np.array(utils.shuffle(matched_CF[k], random_state=seed)
                                     [-1 * self.N:] + utils.shuffle(mismatched_CF[k], random_state=seed)[-1 * self.N:])
                         for k in self.all_classes}

        self.labels = {k: np.concatenate([np.repeat(1, self.N), np.repeat(0, self.N)]) for k in self.all_classes}
        # clean the memory usage

        del (matched_CF, mismatched_CF)

        # build reference for each manipulation
        self.current_class = None
        self.shuffle_all()
        self.set_current_tag(self.all_classes[0])

    def set_current_tag(self, target_class):
        '''
        Update the current tag and reference to current_data and current labels

        :param target_tag: current tag (must be an element of all_classes"
        :return: None
        '''
        self.current_class = target_class
        self.current_data = self.data[self.current_class]
        self.current_labels = self.labels[self.current_class]

    def __len__(self):
        '''

        :return: number of total batches
        '''
        return int(np.ceil(self.N / self.batch_size))

    def prepare_transformed_text(self, file_names):
        all_text = []
        for fn in file_names:
            with open(os.path.join(self.data_root_path, "text", fn), "r", encoding="utf-8") as f:
                all_text.append(f.read())
        return self.tokinizer.texts_to_sequences(all_text)

    def __getitem__(self, index):
        '''

        :return: a complete batch of data
        '''
        start_time = datetime.datetime.now()
        end_index = min((index + 1) * self.batch_size, self.N)
        indices = np.arange(index * self.batch_size, end_index)
        batch_x_file_names = [self.current_data[i] for i in indices]
        batch_x = self.prepare_transformed_text(batch_x_file_names)
        padded_batch_x = pad_sequences(batch_x, maxlen=self.PADDING_LENGTH)
        embedded_padded_x = self.embedder.embed_padded_sequences(padded_batch_x)
        batch_y = [self.current_labels[i] for i in indices]
        categoried_y = to_categorical(batch_y, 2)

        end_time = datetime.datetime.now()
        print("Duration for __getitem__", str(end_time - start_time))
        return embedded_padded_x, np.array(categoried_y)

    def on_epoch_end(self, target_tag=None):
        '''

        Shuffle data under self.current_class .
        :return: null
        '''
        if target_tag is None:
            target_tag = self.current_class
        self.data[target_tag], self.labels[target_tag] = utils.shuffle(self.data[target_tag], self.labels[target_tag])
        self.current_data = self.data[target_tag]
        self.current_labels = self.labels[target_tag]

    def shuffle_all(self):
        for k in self.all_classes:
            self.on_epoch_end(k)

import json, os, spacy
from sector_tagger import *
from collections import OrderedDict
from pprint import pprint
from sklearn.utils import shuffle

data_root_path = "/Users/dqin/Documents/DJ_data/organized_data"
# N = 10 ** 4
nlp = spacy.load("en_core_web_md")
limit = 4 * (10 ** 3)

with open(os.path.join(data_root_path, "unused_test_file_CF_lables_map.json"), "r", encoding="utf-8") as f:
    unused_CF_labels_map = json.load(f, object_pairs_hook=OrderedDict)

with open("SectorTagging/acceptance_prob.json", "r", encoding="utf-8") as f:
    acceptance_prob = json.load(f)

sector_tagger = SectorTagger(nlp, acceptance_prob)
pprint(sector_tagger.acceptance_prob)

results = OrderedDict()
for i, cur_f in enumerate(shuffle(list(unused_CF_labels_map.keys()))[:limit]):
    with open(os.path.join(data_root_path, "text", cur_f), "r", encoding="utf-8") as f:
        cur_text = f.read()
    results[cur_f] = sector_tagger.tag(cur_text)
    print("Finish ", i, "-th", cur_f)

tf_result = []
is_truth_subset_of_result = []
over_count = []
with open(os.path.join("test_results/sector_tagger", "unused_test_results.txt"), "w", encoding="utf-8") as f:
    for fn in results.keys():
        cur_truth = set(unused_CF_labels_map[fn])
        cur_result = results[fn]

        print(fn, "     ", cur_truth == cur_result, "     ", cur_truth, "       ", cur_result, file=f)
        if cur_result.issuperset(cur_truth):
            is_truth_subset_of_result.append(True)
            over_count.append(len(cur_result) - len(cur_truth))
        else:
            is_truth_subset_of_result.append(False)
        tf_result.append(cur_truth == cur_result)
# Now save the result to files...

print("Exact match %:", sum(tf_result) / limit)
print("Superset match %:", sum(is_truth_subset_of_result) / limit)
# calculate the average no. of over-count.?
print("Average over-count", sum(over_count) / (sum(is_truth_subset_of_result) - sum(tf_result)))

'''
    Exact match %: 0.35875
    Superset match %: 0.76925
    Average over-count 1.705237515225335
'''

# save results to local:
for k in results:
    print(k, results[k], file=f)

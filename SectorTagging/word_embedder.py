import numpy as np


class WordEmbedder:
    def __init__(self, nlp_reference, tokenizer):
        self.nlp = nlp_reference
        self.tokenizer = tokenizer
        self.embedding_dim = self.nlp.vocab.vectors.shape[1]
        self.weights = np.zeros(((len(self.tokenizer.word_index) + 1), self.embedding_dim))
        self.__build_weight_matrix()

    def __build_weight_matrix(self):
        for w, i in self.tokenizer.word_index.items():
            try:
                self.weights[i] = self.nlp.vocab[w].vector  # would be zero if the word does not exist
            except KeyError:
                continue

    def embed_one_text(self, input_text):
        seqs = self.tokenizer.texts_to_sequences([input_text])
        return self.embed_padded_sequences(seqs)

    def embed_padded_sequences(self, input_sequences):
        # each of input_sequences must be of same length. i.e. after padding
        return np.array([self.weights[s] for s in input_sequences])


if __name__ == "__main__":
    import spacy

    from keras_preprocessing.text import tokenizer_from_json
    from keras_preprocessing.sequence import pad_sequences
    import json, os
    import numpy as np
    from sklearn import utils

    nlp = spacy.load("en_core_web_md")
    PADDING_LENGTH = 500
    tokenizer_path = "SectorTagging/shared_tokenizer.json"

    with open(tokenizer_path, "r", encoding="utf-8") as f:
        tokenizer = tokenizer_from_json(f.read())

    embedder = WordEmbedder(nlp, tokenizer)

    with open("resources/sample_news.txt", "r", encoding="utf-8") as f:
        sample_text = f.read()
    embedding_results = embedder.embed_one_text(sample_text)
    print(embedding_results.shape)

    with open("resources/sample_news_2.txt", "r", encoding="utf-8") as f:
        sample_text_2 = f.read()
    embedding_results_2 = embedder.embed_one_text(sample_text_2)
    print(embedding_results_2.shape)

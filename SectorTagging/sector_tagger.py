import json, os
import numpy as np
from keras.models import load_model
from SectorTagging.word_embedder import *
from sklearn import utils
from keras_preprocessing.text import tokenizer_from_json
from keras_preprocessing.sequence import pad_sequences


class SectorTagger:
    PADDING_LENGTH = 500
    tokenizer_path = "SectorTagging/shared_tokenizer.json"
    # model_folder_path = "resources/deployed_sector_models"
    model_folder_path = "resources/tagging_models_small"
    taggers = dict()  # for tagging models

    # load taggers
    for n in filter(lambda x: not x.startswith("."), sorted(os.listdir(model_folder_path))):
        model_name = n.split(".")[0]
        taggers[model_name] = load_model(os.path.join(model_folder_path, n))

    def __init__(self, nlp_reference, acceptance_prob=None):
        # load tokenizer
        with open(self.__class__.tokenizer_path, "r", encoding="utf-8") as f:
            self.tokenizer = tokenizer_from_json(f.read())
        self.embedder = WordEmbedder(nlp_reference, self.tokenizer)
        self.set_acceptance_prob(acceptance_prob)

    def set_acceptance_prob(self, acceptance_prob):
        self.acceptance_prob = {k: 0.5 for k in self.__class__.taggers.keys()}  # set default prob = 0.5 for every tag
        if acceptance_prob is None:
            pass
        elif not set(acceptance_prob.keys()).issubset(set(self.__class__.taggers.keys())):
            raise ValueError("Argument of constructor acceptance_prob contains undesired keys. Desired keys:",
                             self.acceptance_prob.keys(), "Received keys:", acceptance_prob.keys())
        else:
            for k in acceptance_prob:
                self.acceptance_prob[k] = acceptance_prob[k]

    def tag(self, input_str):
        tags = set()
        token_indices = self.tokenizer.texts_to_sequences([input_str])
        padded_input = pad_sequences(token_indices, maxlen=self.__class__.PADDING_LENGTH)
        embedded_padded_text = self.embedder.embed_padded_sequences(padded_input)

        for k in self.__class__.taggers.keys():
            cur_result = self.__class__.taggers[k].predict(embedded_padded_text)
            # pred_label = np.argmax(cur_result[0], axis=1)
            if cur_result[0][1] > self.acceptance_prob[k]:  # can be adjusted accordingly
                tags.add(k)
        return tags

    def tag_for_one_class(self, class_name, input_str):
        tags = set()
        token_indices = self.tokenizer.texts_to_sequences([input_str])
        padded_input = pad_sequences(token_indices, maxlen=self.__class__.PADDING_LENGTH)
        embedded_padded_text = self.embedder.embed_padded_sequences(padded_input)

        cur_result = self.__class__.taggers[class_name].predict(embedded_padded_text)
        # pred_label = np.argmax(cur_result[0], axis=1)
        if cur_result[0][1] > self.acceptance_prob[class_name]:  # can be adjusted accordingly
            tags.add(class_name)
        return tags


if __name__ == "__main__":
    import spacy

    nlp = spacy.load("en_core_web_md")
    sector_tagger = SectorTagger(nlp)

    data_root_path = "/Users/dqin/Documents/DJ_data/organized_data"
    with open(os.path.join(data_root_path, "matched_file_names.json"), "r", encoding='utf-8') as f:
        matched_CF = json.load(f)
    with open(os.path.join(data_root_path, "mismatched_file_names.json"), "r", encoding='utf-8') as f:
        mismatched_CF = json.load(f)

    # sectors_to_test = ['SECTOR_CONS_DISCR', 'SECTOR_CONS_STAP', 'SECTOR_ENERGY']
    sectors_to_test = list(matched_CF.keys())

    for cur_sector in sectors_to_test:
        matched_fn = matched_CF[cur_sector]
        mismatched_fn = mismatched_CF[cur_sector]
        test_fns_1 = utils.shuffle(matched_fn, random_state=0)[11000:15000]
        test_fns_2 = utils.shuffle(mismatched_fn, random_state=0)[11000:15000]
        print("*" * 100)
        print("For tag: ", cur_sector)

        for p in np.arange(0.5, 0.8, 0.02):
            sector_tagger.set_acceptance_prob({cur_sector: p})
            tp_count = 0
            tn_count = 0

            for cur_f in test_fns_1:
                with open(os.path.join(data_root_path, "text", cur_f), 'r', encoding="utf-8") as f:
                    if cur_sector in sector_tagger.tag_for_one_class(cur_sector, f.read()):
                        tp_count += 1

            for cur_f in test_fns_2:
                with open(os.path.join(data_root_path, "text", cur_f), 'r', encoding="utf-8") as f:
                    if cur_sector not in sector_tagger.tag_for_one_class(cur_sector, f.read()):
                        tn_count += 1
            print("Current prob:", p)
            print("Total count for +ve:", len(test_fns_1), "   TP:", tp_count)
            print("Total count for -ve:", len(test_fns_2), "   TN:", tn_count)
            print("Overal Accuracy:", (tp_count + tn_count) / (len(test_fns_1) + len(test_fns_2)))
            print("-" * 80)
        print("END of tag: ", cur_sector)
        print("*" * 100)

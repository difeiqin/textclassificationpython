from sector_data_generator import *
from keras_preprocessing.text import tokenizer_from_json

with open("SectorTagging/shared_tokenizer.json", "r", encoding='utf-8') as f:
    tokenizer = tokenizer_from_json(f.read())

generator = SectorDataGenerator(32, tokenizer)

for x in generator:
    print(x)
    break

generator.current_class
generator.current_data
generator.current_labels
generator.labels

type(x)
[len(y) for y in x[0]]
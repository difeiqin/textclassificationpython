import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from data_loader import local_loader
from keras.layers import *
from keras import Model, utils
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import spacy

# nlp = spacy.load('en_core_web_sm')


if __name__ == "__main__":
    data_dict = local_loader.get_classes("ENER")
    # convert all training into one df.

    # data = np.array([data_dict['train.1'], np.repeat(1, len(data_dict['train.1']))]).reshape((-1, 2))
    train_text = np.concatenate([data_dict['train.1'], data_dict['train.0'], data_dict['test.1'], data_dict['test.0']])
    train_labels = np.array(
        [1] * len(data_dict['train.1']) + [0] * len(data_dict['train.0']) + [1] * len(data_dict['test.1']) + [0] * len(
            data_dict['test.0']))

    train_df = pd.DataFrame(columns=['text', 'label']).astype({"text": np.str, "label": np.int})
    train_df['text'] = train_text
    train_df['label'] = train_labels

    tokenizer = Tokenizer()
    # TODO: should apply stop word removal here? Should already tokeinize into list of words, before calling Keras Tokenizer.
    tokenizer.fit_on_texts(train_df['text'])
    vocab = tokenizer.word_index  # index starting from 0, ease 0-padding in future
    x_train, x_test, y_train, y_test = train_test_split(train_df['text'], train_df['label'], test_size=0.1)

    x_train_word_ids = tokenizer.texts_to_sequences(x_train)
    x_test_word_ids = tokenizer.texts_to_sequences(x_test)
    # word_counts = list(map(lambda x: len(x), x_train_word_ids)) # TODO: plot to visualize the freq. distribution?
    PAD_LENGTH = 500
    x_train_padded_ids = pad_sequences(x_train_word_ids, maxlen=PAD_LENGTH)
    x_test_padded_ids = pad_sequences(x_test_word_ids, maxlen=PAD_LENGTH)

    '''
    ------------ data cleaning done so far ------------
    '''

nlp = spacy.load('en_core_web_md')
emb_dimension = len(nlp.vocab['apple'].vector)
input_dim = PAD_LENGTH
output_dim = len(set(y_train.values))

embedding_matrix = np.zeros((len(vocab) + 1, emb_dimension))

for w, i in vocab.items():
    try:
        embedding_matrix[i] = nlp.vocab[w].vector
    except KeyError:
        continue


def TextCNN_model_1(x_train, y_train, x_test, y_test):
    input_layer = Input(shape=(PAD_LENGTH,), dtype=np.float64)

    embedding_layer = Embedding(len(vocab) + 1, emb_dimension, input_length=PAD_LENGTH, weights=[embedding_matrix],
                                trainable=False)
    embed = embedding_layer(input_layer)

    cnn1 = Conv1D(64, 3, padding="same", strides=1, activation='relu')(embed)
    cnn1 = MaxPool1D(pool_size=PAD_LENGTH - 2)(cnn1)  # pool_size, max pooling windows
    cnn2 = Conv1D(64, 4, padding="same", strides=1, activation='relu')(embed)
    cnn2 = MaxPool1D(pool_size=PAD_LENGTH - 3)(cnn2)  # pool_size, max pooling windows
    cnn3 = Conv1D(64, 5, padding="same", strides=1, activation='relu')(embed)
    cnn3 = MaxPool1D(pool_size=PAD_LENGTH - 4)(cnn3)  # pool_size, max pooling windows

    concat_layer = concatenate([cnn1, cnn2, cnn3], axis=-1)  # Not Concatenate...
    flat = Flatten()(concat_layer)
    drop = Dropout(rate=0.5)(flat)
    output_layer = Dense(output_dim, activation='softmax')(drop)
    model = Model(inputs=input_layer, outputs=output_layer)

    # tokenizer.texts_to_sequences(train_df['text'][0])[:10] # WRONG, need to be a list of list, otherwise it's char-level splitting.


    model.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
    one_hot_labels = utils.to_categorical(y_train, output_dim)

    model.fit(x_train_padded_ids, one_hot_labels, batch_size=200, epochs=10)

    # y_test
    # x_test_padded_ids.shape
    pred_result = model.predict(x_test_padded_ids)
    pred_labels = np.argmax(pred_result, axis=1)

    from sklearn import metrics
    metrics.accuracy_score(y_test, pred_labels)
    metrics.precision_recall_curve(y_test, pred_labels)
    metrics.f1_score(y_test, pred_labels)


    from keras.models import save_model, load_model
    # model.save('model_001_64.h5')

    model = load_model("/Users/dqin/PycharmProjects/TextClassification/model_001_64.h5")
    from keras.utils import plot_model
    plot_model(model, to_file='model_001_64.png', show_shapes=True, show_layer_names=False)
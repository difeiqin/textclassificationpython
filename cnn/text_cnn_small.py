import sys

sys.path.insert(1, "/Users/dqin/PycharmProjects/TextClassification")
from keras.layers import *
from keras import Model, utils


def TextCNN_model_small(input_dim, output_dim, embedding_dim, max_pool_size=None):
    '''

    The TextCNN_model that has the input text already padded and embedded using some word2vec/glove.
    i.e. this model does not have embedding layer.

    :param input_dim: the padding length
    :param output_dim: output layer dimension, 2 for binary classification
    :param embedding_weights: embedding matrix, N_vocab * d_dimension
    :param max_pool_size: size for max pooling
    :return: a TextCNN model
    '''

    if max_pool_size is None:
        max_pool_size = input_dim
    input_layer = Input(shape=(input_dim, embedding_dim), dtype=np.float32)

    cnn1 = Conv1D(filters=32, kernel_size=3, padding="same", strides=1, activation="relu")(input_layer)
    cnn1 = MaxPool1D(pool_size=max_pool_size)(cnn1)
    cnn2 = Conv1D(filters=32, kernel_size=4, padding="same", strides=1, activation="relu")(input_layer)
    cnn2 = MaxPool1D(pool_size=max_pool_size)(cnn2)
    cnn3 = Conv1D(filters=32, kernel_size=5, padding="same", strides=1, activation="relu")(input_layer)
    cnn3 = MaxPool1D(pool_size=max_pool_size)(cnn3)
    cnn4 = Conv1D(filters=32, kernel_size=6, padding="same", strides=1, activation="relu")(input_layer)
    cnn4 = MaxPool1D(pool_size=max_pool_size)(cnn4)

    cnn = concatenate([cnn1, cnn2, cnn3, cnn4], axis=-1)
    flat = Flatten()(cnn)
    drop = Dropout(rate=0.5)(flat)
    dense1 = Dense(200, activation="relu")(drop)
    drop2 = Dropout(rate=0.5)(dense1)
    dense2 = Dense(output_dim, activation="softmax")(drop2)
    model = Model(inputs=input_layer, outputs=dense2)
    return model

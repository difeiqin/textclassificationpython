import pandas as pd
import sys
sys.path.insert(1, "/Users/dqin/PycharmProjects/TextClassification")
import numpy as np
from sklearn.model_selection import train_test_split
from data_loader import local_loader
from keras.layers import *
from keras import Model, utils
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import plot_model
from sklearn import metrics
import spacy


def convert_to_df(data_dict, mix_test=True):
    '''

    :param data_dict: A dictionary containing data read from loader, either 1-layer or 2-layer dictionary.
    :return: dictionary of df, with key being class name, df containing data
    '''

    result_dict = dict()
    for k, v_dict in data_dict.items():
        text = np.concatenate([v_dict['train.0'], v_dict['train.1']])
        labels = np.concatenate([[0] * len(v_dict['train.0']), [1] * len(v_dict['train.1'])])
        if mix_test:
            text = np.concatenate([text, v_dict['test.0'], v_dict['test.1']])
            labels = np.concatenate([labels, [0] * len(v_dict['test.0']), [1] * len(v_dict['test.1'])])
        temp_df = pd.DataFrame(columns=["text", "label"]).astype({"text": np.str, "label": np.int})
        temp_df['text'] = text
        temp_df['label'] = labels
        result_dict[k] = temp_df
    return result_dict


def get_embedding_weights(vocab, nlp):
    """

    :param vocab: dictionary, result of tokenization. Containing word -> index mapping.
    :param nlp: the spacy object to use for wordEmbedding. (By default, use GloVe from Spacy)
    :return: a matrix of dimension (len(nlp)+1, embedding_dimension)
    """
    embedding_dim = nlp.vocab.vectors.shape[1]
    weights = np.zeros(((len(vocab) + 1), embedding_dim))
    for w, i in vocab.items():
        try:
            weights[i] = nlp.vocab[w].vector  # would be zero if the word does not exist
        except KeyError:
            continue
    return weights


def TextCNN_model_1(input_dim, output_dim, embedding_weights, max_pool_size=None):
    if max_pool_size is None:
        max_pool_size = input_dim
    input_layer = Input(shape=(input_dim,), dtype=np.float64)
    embedding_layer = Embedding(input_dim=embedding_weights.shape[0], output_dim=embedding_weights.shape[1],
                                weights=[embedding_weights], trainable=True)(input_layer)

    cnn1 = Conv1D(filters=32, kernel_size=3, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn1 = MaxPool1D(pool_size=max_pool_size)(cnn1)
    cnn2 = Conv1D(filters=32, kernel_size=4, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn2 = MaxPool1D(pool_size=max_pool_size)(cnn2)
    cnn3 = Conv1D(filters=32, kernel_size=5, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn3 = MaxPool1D(pool_size=max_pool_size)(cnn3)
    cnn4 = Conv1D(filters=32, kernel_size=6, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn4 = MaxPool1D(pool_size=max_pool_size)(cnn4)

    cnn = concatenate([cnn1, cnn2, cnn3, cnn4], axis=-1)
    flat = Flatten()(cnn)
    drop = Dropout(rate=0.5)(flat)
    dense1 = Dense(200, activation="relu")(drop)
    drop2 = Dropout(rate=0.5)(dense1)
    dense2 = Dense(output_dim, activation="softmax")(drop2)
    model = Model(inputs=input_layer, outputs=dense2)
    return model


def TextCNN_model_original(input_dim, output_dim, embedding_weights, max_pool_size=None):
    if max_pool_size is None:
        max_pool_size = input_dim
    input_layer = Input(shape=(input_dim,), dtype=np.float64)
    embedding_layer = Embedding(input_dim=embedding_weights.shape[0], output_dim=embedding_weights.shape[1],
                                weights=[embedding_weights], trainable=True)(input_layer)

    cnn1 = Conv1D(filters=100, kernel_size=3, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn1 = MaxPool1D(pool_size=max_pool_size)(cnn1)
    cnn2 = Conv1D(filters=100, kernel_size=4, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn2 = MaxPool1D(pool_size=max_pool_size)(cnn2)
    cnn3 = Conv1D(filters=100, kernel_size=5, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn3 = MaxPool1D(pool_size=max_pool_size)(cnn3)
    cnn4 = Conv1D(filters=100, kernel_size=6, padding="same", strides=1, activation="relu")(embedding_layer)
    cnn4 = MaxPool1D(pool_size=max_pool_size)(cnn4)

    cnn = concatenate([cnn1, cnn2, cnn3, cnn4], axis=-1)
    flat = Flatten()(cnn)
    drop = Dropout(rate=0.5)(flat)
    dense1 = Dense(output_dim, activation="softmax", kernel_regularizer=regularizers.l2(0.01))(drop)
    # dense1 = Dense(output_dim, activation="softmax")(drop)
    model = Model(inputs=input_layer, outputs=dense1)
    return model


if __name__ == "__main__":
    nlp = spacy.load("en_core_web_md")

    all_classes = local_loader.list_all_classes()
    # data_dict = local_loader.get_classes(all_classes[:15])
    data_dict = local_loader.get_classes(["INDS"])
    df_dict = convert_to_df(data_dict)
    EPOCHS = 25

    for class_name in df_dict.keys():
        print("Current class: ", class_name)
        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(df_dict[class_name]['text'])

        vocab = tokenizer.word_index
        embedding_weights = get_embedding_weights(vocab, nlp)
        X_train, X_test, y_train, y_test = train_test_split(df_dict[class_name]['text'], df_dict[class_name]['label'],
                                                            test_size=0.1)

        PADDING_LENGTH = 500
        X_train_padded_ids = pad_sequences(tokenizer.texts_to_sequences(X_train), maxlen=PADDING_LENGTH)
        X_test_padded_ids = pad_sequences(tokenizer.texts_to_sequences(X_test), maxlen=PADDING_LENGTH)

        # -------  constructing model ------- #
        model1 = TextCNN_model_1(PADDING_LENGTH, 2, embedding_weights, max_pool_size=10)
        # plot_model(model1, show_shapes=True, show_layer_names=False)

        # -------  model training.... ------- #
        one_hot_labels = utils.to_categorical(y_train, 2)
        model1.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
        model1.fit(X_train_padded_ids, one_hot_labels, batch_size=100, epochs=EPOCHS)

        # ------- model prediction ------- #
        y_pred = model1.predict(X_test_padded_ids)
        pred_labels = np.argmax(y_pred, axis=1)

        with open("CNN_all_classes_evaluation.txt", "a+", encoding="utf-8") as f:
            f.write(class_name + ": ")
            f.write("   Accuracy: " + str(metrics.accuracy_score(y_test, pred_labels)))
            f.write("   f1 score: " + str(metrics.f1_score(y_test, pred_labels)))
            f.write("\n")

    # ------- save model to local ------- #
    # model1.save("resources/model_003_10_pool_size_two_dense_layers_0_83_acc.h5")

import csv, os, sys
import pandas as pd


class DJCodeTranslator:
    def __init__(self):
        self.industry_dict = dict()
        self.subject_dict = dict()
        self.region_dict = dict()

        with open("DJ_code_translator/industries.csv", 'r', encoding='utf-8') as f:
            f.read()  # skip the first line
            for l in csv.reader(f):
                self.industry_dict[l[0]] = l[1]

        with open("DJ_code_translator/news_subjects.csv", 'r', encoding='utf-8') as f:
            f.read()  # skip the first line
            for l in csv.reader(f):
                self.subject_dict[l[0]] = l[1]

        with open("DJ_code_translator/regions.csv", 'r', encoding='utf-8') as f:
            f.read()  # skip the first line
            for l in csv.reader(f):
                self.region_dict[l[0]] = l[1]

    def translate_industry(self, one_code):
        return self.industry_dict.get(one_code, None)

    def translate_subject(self, one_code):
        return self.subject_dict.get(one_code, None)

    def translate_region(self, one_code):
        return self.region_dict.get(one_code, None)

    def translate_industries(self, code_strings):
        if pd.isna(code_strings):
            return None
        codes = [x for x in code_strings.split(",") if len(x) > 0]
        return [self.translate_industry(c) for c in codes]

    def translate_subjects(self, code_strings):
        if pd.isna(code_strings):
            return None
        codes = [x for x in code_strings.split(",") if len(x) > 0]
        return [self.translate_subject(c) for c in codes]

    def translate_regions(self, code_strings):
        if pd.isna(code_strings):
            return None
        codes = [x for x in code_strings.split(",") if len(x) > 0]
        return [self.translate_region(c) for c in codes]

    def translate_one_row(self, input_r):
        results = dict()
        results['industry_codes'] = self.translate_industries(input_r['industry_codes'])
        results['subject_codes'] = self.translate_subjects(input_r['subject_codes'])
        results['region_codes'] = self.translate_regions(input_r['region_codes'])
        return results


if __name__ == "__main__":
    dj_code_translator = DJCodeTranslator()
    dj_code_translator.translate_one_row()